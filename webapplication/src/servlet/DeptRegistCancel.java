package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/dept_regist_cancel")

public class DeptRegistCancel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("dept_id", request.getParameter("dept_id"));
		request.setAttribute("dept_name", request.getParameter("dept_name"));
		request.getRequestDispatcher("/jsp/DeptRegist.jsp").forward(request, response);
	}

}
