package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dao.EmpDAO;
import dto.Dept;
import dto.Emp;

@WebServlet("/dept_detail")
public class DeptDetailView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		String deptId = request.getParameter("dept_id");
		ArrayList<Emp> list = new EmpDAO().selectByDept(deptId);
		Dept detailDept = new DeptDAO().select(deptId);

		if (detailDept == null) {
			request.setAttribute("errmsg", "部署はすでに削除されました");
			request.getRequestDispatcher("/jsp/DeptList.jsp").forward(request, response);
			return;
		}

		session.setAttribute("detail_dept", detailDept);
		request.setAttribute("emplist_bydept", list);

		request.getRequestDispatcher("/jsp/DeptDetail.jsp").forward(request, response);
	}

}
