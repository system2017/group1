package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.Emp;

@WebServlet("/emp_update")
public class EmpUpdateView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Emp emp = (Emp) session.getAttribute("detail_emp");
		request.setAttribute("emp_name", emp.getEmpName());
		request.setAttribute("emp_mail", emp.getMail());
		request.setAttribute("emp_birthday", emp.getBirthday());
		request.setAttribute("emp_salary", emp.getSalary());
		request.setAttribute("emp_dept", emp.getDeptId());
		request.setAttribute("emp_password", emp.getPassword());
		request.getRequestDispatcher("/jsp/EmpUpdate.jsp").forward(request, response);
	}

}
