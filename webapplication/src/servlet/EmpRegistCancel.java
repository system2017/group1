package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/emp_regist_cancel")
public class EmpRegistCancel extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setAttribute("emp_name", request.getParameter("emp_name"));
		request.setAttribute("emp_id", request.getParameter("emp_id"));
		request.setAttribute("emp_mail", request.getParameter("emp_mail"));
		request.setAttribute("emp_birthday", request.getParameter("emp_birthday"));
		request.setAttribute("emp_salary", request.getParameter("emp_salary"));
		request.setAttribute("emp_dept", request.getParameter("emp_dept"));
		request.getRequestDispatcher("/jsp/EmpRegist.jsp").forward(request, response);
	}

}
