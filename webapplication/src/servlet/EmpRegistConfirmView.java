package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmpDAO;
import dto.Dept;

@WebServlet("/emp_regist_confirm")
public class EmpRegistConfirmView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String emp_id = request.getParameter("emp_id");
		String emp_name = request.getParameter("emp_name");
		String mail = request.getParameter("mail");
		String birthday = request.getParameter("birthday");
		String salary = request.getParameter("salary");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");
		String dept_id = request.getParameter("dept_id");

		request.setAttribute("emp_id", emp_id);
		request.setAttribute("emp_name", emp_name);
		request.setAttribute("emp_mail", mail);
		request.setAttribute("emp_birthday", birthday);
		request.setAttribute("emp_salary", salary);
		request.setAttribute("emp_dept", dept_id);
		request.setAttribute("password", password);

		boolean deptcheck = false;
		@SuppressWarnings("unchecked")
		ArrayList<Dept> deptlist = (ArrayList<Dept>) request.getServletContext().getAttribute("dept_list");
		for (int i = 0; i < deptlist.size(); i++) {
			if (dept_id.equals(deptlist.get(i).getDeptId())) {
				deptcheck = true;
				break;
			}
		}

		if (!deptcheck) {
			request.setAttribute("errMsg", "部署が削除されました。再度選択してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (emp_id.equals("") || emp_name.equals("") || birthday.equals("") || salary.equals("") || password.equals("")
				|| passwordConfirm.equals("")) {
			request.setAttribute("errMsg", "未入力項目があります");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		EmpDAO empDAO = new EmpDAO();

		if (empDAO.select(emp_id) != null) {
			request.setAttribute("errMsg", "すでに登録されているIDです。再度入力してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (empDAO.selectByMail(mail) != null) {
			request.setAttribute("errMsg", "すでに登録されているメールアドレスです。再度入力してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		try {
			Integer.parseInt(salary);
		} catch (NumberFormatException e) {
			request.setAttribute("errMsg", "給与が上限値を超えています");

			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (!password.equals(passwordConfirm)) {
			request.setAttribute("errMsg", "パスワードが一致していません");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegistConfirm.jsp");
		requestDispatcher.forward(request, response);
	}

}