package servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmpDAO;
import dto.Dept;
import dto.Emp;

@WebServlet("/emp_regist_complete")
public class EmpRegistCompleteView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String emp_id = request.getParameter("emp_id");
		String emp_name = request.getParameter("emp_name");
		String mail = request.getParameter("emp_mail");
		String birthday = request.getParameter("emp_birthday");
		String salary = request.getParameter("emp_salary");
		String dept_id = request.getParameter("emp_dept");

		boolean deptcheck = false;
		boolean mailcheck = true;
		boolean idcheck = true;

		@SuppressWarnings("unchecked")
		ArrayList<Dept> deptlist = (ArrayList<Dept>) request.getServletContext().getAttribute("dept_list");
		@SuppressWarnings("unchecked")
		ArrayList<Emp> emplist = (ArrayList<Emp>) request.getServletContext().getAttribute("emp_list");

		for (int i = 0; i < deptlist.size(); i++) {
			if (dept_id.equals(deptlist.get(i).getDeptId())) {
				deptcheck = true;
				break;
			}
		}

		for (int i = 0; i < emplist.size(); i++) {
			if (emp_id.equals(emplist.get(i).getEmpId())) {
				idcheck = false;
				break;
			}
		}

		for (int i = 0; i < emplist.size(); i++) {
			if (mail.equals(emplist.get(i).getMail())) {
				mailcheck = false;
				break;
			}
		}

		if (!deptcheck || !mailcheck || !idcheck) {
			request.setAttribute("emp_id", emp_id);
			request.setAttribute("emp_name", emp_name);
			request.setAttribute("emp_mail", mail);
			request.setAttribute("emp_birthday", birthday);
			request.setAttribute("emp_salary", salary);
			if (!idcheck)
				request.setAttribute("errMsg", "すでに登録されているIDです。再度入力してください");
			else if (!mailcheck)
				request.setAttribute("errMsg", "すでに登録されているメールアドレスです。再度入力してください");
			else
				request.setAttribute("errMsg", "部署が削除されました。再度選択してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		java.sql.Date date = null;
		try {
			date = new java.sql.Date(sdf.parse(request.getParameter("emp_birthday")).getTime());
		} catch (ParseException e1) {
			e1.printStackTrace();
		}

		Emp emp = new Emp(request.getParameter("emp_id"), request.getParameter("emp_name"),
				request.getParameter("emp_mail"), date, Integer.parseInt(request.getParameter("emp_salary")),
				request.getParameter("password"), request.getParameter("emp_dept"));

		EmpDAO empDAO = new EmpDAO();

		if (!empDAO.regist(emp)) {
			request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
			return;
		}

		request.getServletContext().setAttribute("emp_list", new EmpDAO().selectAll());
		request.setAttribute("emp", emp);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpRegistComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
