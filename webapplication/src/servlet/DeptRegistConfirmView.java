package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DeptDAO;

@WebServlet("/dept_regist_confirm")
public class DeptRegistConfirmView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String dept_id = request.getParameter("dept_id");
		String dept_name = request.getParameter("dept_name");

		request.setAttribute("dept_name", dept_name);
		request.setAttribute("dept_id", dept_id);

		if (dept_id.equals("") || dept_name.equals("")) {
			request.setAttribute("errMsg", "未入力項目があります");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		DeptDAO deptDAO = new DeptDAO();

		if (deptDAO.select(dept_id) != null) {
			request.setAttribute("errMsg", "すでに登録されているIDです。再度入力してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptRegistConfirm.jsp");
		requestDispatcher.forward(request, response);
	}

}
