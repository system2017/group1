package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/dept_update_confirm")
public class DeptUpdateConfirmView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String deptName = request.getParameter("dept_name");
		request.setAttribute("dept_name", deptName);
		if (deptName.equals("")) {
			request.setAttribute("errmsg", "未記入項目があります");
			request.getRequestDispatcher("/jsp/DeptUpdate.jsp").forward(request, response);
			return;
		}
		request.getRequestDispatcher("/jsp/DeptUpdateConfirm.jsp").forward(request, response);
	}

}