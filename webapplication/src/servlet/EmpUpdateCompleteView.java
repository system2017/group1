package servlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Dept;
import dto.Emp;

@WebServlet("/emp_update_complete")
public class EmpUpdateCompleteView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Emp emp = null;
		Emp detailEmp = (Emp) session.getAttribute("detail_emp");
		EmpDAO empdao = new EmpDAO();

		String empId = ((Emp) session.getAttribute("detail_emp")).getEmpId();

		if (empdao.select(empId) == null) {
			request.setAttribute("errmsg", "社員はすでに削除されました");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpList.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		String empName = request.getParameter("emp_name");
		request.setAttribute("emp_name", empName);
		String empMail = request.getParameter("emp_mail");
		request.setAttribute("emp_mail", empMail);
		String empBirthday = request.getParameter("emp_birthday");
		request.setAttribute("emp_birthday", empBirthday);
		String empSalary = request.getParameter("emp_salary");
		request.setAttribute("emp_salary", empSalary);
		String empDept = request.getParameter("emp_dept");
		request.setAttribute("emp_dept", empDept);

		Emp emp2 = new EmpDAO().selectByMail(empMail);

		if (emp2 != null && !emp2.getEmpId().equals(detailEmp.getEmpId())) {
			request.setAttribute("errmsg", "すでに登録されているメールアドレスです。再度入力してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpUpdate.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		boolean deptcheck = false;

		@SuppressWarnings("unchecked")
		ArrayList<Dept> deptlist = (ArrayList<Dept>) request.getServletContext().getAttribute("dept_list");
		for (int i = 0; i < deptlist.size(); i++) {
			if (empDept.equals(deptlist.get(i).getDeptId())) {
				deptcheck = true;
				break;
			}
		}

		if (!deptcheck) {
			request.setAttribute("errmsg", "部署が削除されました。再度選択してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpUpdate.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		try {
			emp = new Emp(((Emp) session.getAttribute("detail_emp")).getEmpId(), request.getParameter("emp_name"),
					request.getParameter("emp_mail"),
					new java.sql.Date(
							new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("emp_birthday")).getTime()),
					Integer.parseInt(request.getParameter("emp_salary")), request.getParameter("emp_password"),
					request.getParameter("emp_dept"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (!empdao.update(emp)) {
			request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
			return;
		}
		request.getServletContext().setAttribute("emp_list", empdao.selectAll());
		request.setAttribute("emp", emp);
		request.setAttribute("passwd_change", request.getParameter("passwd_change"));
		request.getRequestDispatcher("/jsp/EmpUpdateComplete.jsp").forward(request, response);
	}

}
