package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DeptDAO;
import dto.Dept;

@WebServlet("/dept_regist_complete")
public class DeptRegistCompleteView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String deptid = request.getParameter("dept_id");
		String deptname = request.getParameter("dept_name");

		boolean idcheck = true;

		@SuppressWarnings("unchecked")
		ArrayList<Dept> deptlist = (ArrayList<Dept>) request.getServletContext().getAttribute("dept_list");

		for (int i = 0; i < deptlist.size(); i++) {
			if (deptid.equals(deptlist.get(i).getDeptId())) {
				idcheck = false;
				break;
			}
		}

		if (!idcheck) {
			request.setAttribute("dept_id", deptid);
			request.setAttribute("dept_name", deptname);
			request.setAttribute("errMsg", "すでに登録されているIDです。再度入力してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptRegist.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		Dept dept = new Dept(deptid, deptname);
		DeptDAO deptDAO = new DeptDAO();

		if (deptDAO.regist(dept)) {
			request.getServletContext().setAttribute("dept_list", new DeptDAO().selectAll());
		} else {
			request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
			return;
		}
		request.setAttribute("dept", dept);
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptRegistComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
