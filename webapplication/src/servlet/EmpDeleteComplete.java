package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Emp;

@WebServlet("/emp_delete_complete")
public class EmpDeleteComplete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		Emp emp = (Emp) session.getAttribute("detail_emp");
		String empId = emp.getEmpId();

		@SuppressWarnings("unchecked")
		ArrayList<Emp> empList = (ArrayList<Emp>) request.getServletContext().getAttribute("emp_list");
		boolean mark = false;

		for (int i = 0; i < empList.size(); i++) {
			if (emp.getEmpId().equals(empList.get(i).getEmpId())) {
				mark = true;
				break;
			}
		}

		if (!mark) {
			request.setAttribute("errmsg", "社員はすでに削除されました");
			request.getRequestDispatcher("/jsp/EmpList.jsp").forward(request, response);
			return;
		}

		EmpDAO empDAO = new EmpDAO();

		if (!empDAO.delete(empId)) {
			request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
			return;
		}

		request.getServletContext().setAttribute("emp_list", new EmpDAO().selectAll());

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpDeleteComplete.jsp");
		requestDispatcher.forward(request, response);
	}

}
