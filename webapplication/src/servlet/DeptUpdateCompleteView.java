package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dto.Dept;

@WebServlet("/dept_update_complete")
public class DeptUpdateCompleteView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		DeptDAO deptdao = new DeptDAO();

		String deptId = ((Dept) session.getAttribute("detail_dept")).getDeptId();

		if (deptdao.select(deptId) == null) {
			request.setAttribute("errmsg", "部署はすでに削除されました");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptList.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		Dept dept = new Dept(((Dept) session.getAttribute("detail_dept")).getDeptId(),
				request.getParameter("dept_name"));

		if (!deptdao.update(dept)) {
			request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
			return;
		}
		request.getServletContext().setAttribute("dept_list", deptdao.selectAll());
		request.setAttribute("dept", dept);
		request.getRequestDispatcher("/jsp/DeptUpdateComplete.jsp").forward(request, response);
	}

}
