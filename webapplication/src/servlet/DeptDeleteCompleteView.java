package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dao.EmpDAO;
import dto.Dept;
import dto.Emp;

@WebServlet("/dept_delete_complete")
public class DeptDeleteCompleteView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);
		Dept dept = (Dept) session.getAttribute("detail_dept");
		String deptId = dept.getDeptId();
		DeptDAO deptDAO = new DeptDAO();
		EmpDAO empDAO = new EmpDAO();

		@SuppressWarnings("unchecked")
		ArrayList<Dept> deptList = (ArrayList<Dept>) request.getServletContext().getAttribute("dept_list");
		boolean mark = false;

		for (int i = 0; i < deptList.size(); i++) {
			if (dept.getDeptId().equals(deptList.get(i).getDeptId())) {
				mark = true;
				break;
			}
		}

		if (!mark) {
			request.setAttribute("errmsg", "部署はすでに削除されました");
			request.getRequestDispatcher("/jsp/DeptList.jsp").forward(request, response);
			return;
		}

		ArrayList<Emp> list = empDAO.selectByDept(deptId);

		if (list.size() == 0) {
			if (deptDAO.delete(deptId)) {
				request.getServletContext().setAttribute("dept_list", new DeptDAO().selectAll());
			} else {
				request.getRequestDispatcher("/jsp/error.jsp").forward(request, response);
				return;
			}

		} else {
			request.setAttribute("errm", "社員がいる部署は削除できません。");
			request.setAttribute("dept_member", true);
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptDelete.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/DeptDeleteComplete.jsp");
		requestDispatcher.forward(request, response);

	}

}
