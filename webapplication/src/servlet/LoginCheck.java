package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.DeptDAO;
import dao.EmpDAO;
import dto.Emp;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet(name = "logincheck", urlPatterns = "/login_check")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("name");
		String pass = request.getParameter("password");

		EmpDAO empDAO = new EmpDAO();
		Emp emp = empDAO.select(id);
		if (emp == null) {
			request.setAttribute("loginErr", "IDもしくはパスワードが不正です");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Login.jsp");
			requestDispatcher.forward(request, response);
			return;
		}
		String getpass = emp.getPassword();

		if (pass.equals(getpass)) {
			HttpSession session = request.getSession(true);
			session.setAttribute("login_status", emp);
			response.sendRedirect("/webapplication/menu");
			request.getServletContext().setAttribute("dept_list", new DeptDAO().selectAll());
			request.getServletContext().setAttribute("emp_list", new EmpDAO().selectAll());
		} else {
			request.setAttribute("loginErr", "IDもしくはパスワードが不正です");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/Login.jsp");
			requestDispatcher.forward(request, response);
		}
	}

}
