package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Dept;
import dto.Emp;

@WebServlet("/emp_update_confirm")
public class EmpUpdateConfirmView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		Emp detailEmp = (Emp) session.getAttribute("detail_emp");
		String empName = request.getParameter("emp_name");
		request.setAttribute("emp_name", empName);
		String empMail = request.getParameter("emp_mail");
		request.setAttribute("emp_mail", empMail);
		String empBirthday = request.getParameter("emp_birthday");
		request.setAttribute("emp_birthday", empBirthday);
		String empSalary = request.getParameter("emp_salary");
		request.setAttribute("emp_salary", empSalary);
		String empDept = request.getParameter("emp_dept");
		request.setAttribute("emp_dept", empDept);
		String empOldPw = request.getParameter("emp_old_password");
		String empNewPw = request.getParameter("emp_new_password");
		String empNewPwConfirm = request.getParameter("emp_new_password_confirm");

		// エラーチェック

		boolean deptcheck = false;
		@SuppressWarnings("unchecked")
		ArrayList<Dept> deptlist = (ArrayList<Dept>) request.getServletContext().getAttribute("dept_list");
		for (int i = 0; i < deptlist.size(); i++) {
			if (empDept.equals(deptlist.get(i).getDeptId())) {
				deptcheck = true;
				break;
			}
		}

		if (!deptcheck) {
			request.setAttribute("errmsg", "部署が削除されました。再度選択してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpUpdate.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		if (empName.equals("") || empMail.equals("") || empBirthday.equals("") || empSalary.equals("")
				|| empOldPw.equals("")) {
			request.setAttribute("errmsg", "未入力項目があります");
			request.getRequestDispatcher("/jsp/EmpUpdate.jsp").forward(request, response);
			return;
		}

		Emp emp = new EmpDAO().selectByMail(empMail);

		if (emp != null && !emp.getEmpId().equals(detailEmp.getEmpId())) {
			request.setAttribute("errmsg", "すでに登録されているメールアドレスです。再度入力してください");
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("/jsp/EmpUpdate.jsp");
			requestDispatcher.forward(request, response);
			return;
		}

		try {
			Integer.parseInt(empSalary);
		} catch (NumberFormatException e) {
			request.setAttribute("errmsg", "給与が上限値を超えています");
			request.getRequestDispatcher("/jsp/EmpUpdate.jsp").forward(request, response);
			return;
		}

		if (!empOldPw.equals(detailEmp.getPassword())) {
			request.setAttribute("errmsg", "パスワードが間違っています");
			request.getRequestDispatcher("/jsp/EmpUpdate.jsp").forward(request, response);
			return;
		}

		if (!empNewPw.equals(empNewPwConfirm)) {
			request.setAttribute("errmsg", "新パスワードが一致していません");
			request.getRequestDispatcher("/jsp/EmpUpdate.jsp").forward(request, response);
			return;
		}

		if (empNewPw.equals("") && empNewPwConfirm.equals("")) {
			request.setAttribute("emp_password", empOldPw);
			request.setAttribute("passwd_change", "なし");
		} else {
			request.setAttribute("emp_password", empNewPw);
			request.setAttribute("passwd_change", "あり");
		}

		request.getRequestDispatcher("/jsp/EmpUpdateConfirm.jsp").forward(request, response);
	}

}
