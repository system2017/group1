package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EmpDAO;
import dto.Emp;

@WebServlet("/emp_detail")
public class EmpDetailView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String empId = request.getParameter("emp_id");
		EmpDAO empDAO = new EmpDAO();
		Emp detail_emp = empDAO.select(empId);

		if (detail_emp == null) {
			request.setAttribute("errmsg", "社員はすでに削除されました");
			request.getRequestDispatcher("/jsp/EmpList.jsp").forward(request, response);
			return;
		}

		session.setAttribute("detail_emp", detail_emp);
		request.setAttribute("deteil_emp_req", detail_emp);

		RequestDispatcher rd = request.getRequestDispatcher("/jsp/EmpDetail.jsp");
		rd.forward(request, response);

	}

}
