package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dto.Dept;

@WebServlet("/dept_update")
public class DeptUpdateView extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		Dept dept = (Dept) session.getAttribute("detail_dept");
		request.setAttribute("dept_id", dept.getDeptId());
		request.setAttribute("dept_name", dept.getDeptName());
		request.getRequestDispatcher("/jsp/DeptUpdate.jsp").forward(request, response);
	}

}
