package dto;

import java.sql.Date;

public class Emp {

	private String empId;
	private String empName;
	private String mail;
	private Date birthday;
	private int salary;
	private String password;
	private String deptId;

	public Emp(String empId, String empName, String mail, Date birthday, int salary, String password, String deptId) {
		this.empId = empId;
		this.empName = empName;
		this.mail = mail;
		this.birthday = birthday;
		this.salary = salary;
		this.password = password;
		this.deptId = deptId;
	}

	public String getEmpId() {
		return empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDeptId() {
		return deptId;
	}

	public void setDeptId(String deptId) {
		this.deptId = deptId;
	}

}
