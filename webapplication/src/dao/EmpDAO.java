package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.Emp;
import util.ConnectionManager;

public class EmpDAO {

	public ArrayList<Emp> selectAll() {

		ConnectionManager manager = new ConnectionManager();
		ArrayList<Emp> list = new ArrayList<Emp>();
		String sql = "select * from emp order by empid";

		try {
			ResultSet result = manager.getConnection().prepareStatement(sql).executeQuery();
			while (result.next())
				list.add(new Emp(result.getString(1), result.getString(2), result.getString(3), result.getDate(4),
						result.getInt(5), result.getString(6), result.getString(7)));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		return list;
	}

	public ArrayList<Emp> selectByDept(String deptId) {
		ConnectionManager manager = new ConnectionManager();
		ArrayList<Emp> list = new ArrayList<Emp>();
		String sql = "select * from emp join dept on emp.deptid = dept.deptid where dept.deptid = ? order by empid";

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, deptId);
			ResultSet result = prepareStatement.executeQuery();
			while (result.next())
				list.add(new Emp(result.getString(1), result.getString(2), result.getString(3), result.getDate(4),
						result.getInt(5), result.getString(6), result.getString(7)));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		return list;
	}

	public Emp selectByMail(String mail) {
		ConnectionManager manager = new ConnectionManager();
		Emp emp = null;
		String sql = "select * from emp where mail = ? order by empid";

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, mail);
			ResultSet result = prepareStatement.executeQuery();
			if (result.next())
				emp = new Emp(result.getString(1), result.getString(2), result.getString(3), result.getDate(4),
						result.getInt(5), result.getString(6), result.getString(7));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		return emp;
	}

	public Emp select(String empId) {

		ConnectionManager manager = new ConnectionManager();
		Emp emp = null;
		String sql = "select * from emp where empid = ? order by empid";

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, empId);
			ResultSet result = prepareStatement.executeQuery();
			if (result.next())
				emp = new Emp(result.getString(1), result.getString(2), result.getString(3), result.getDate(4),
						result.getInt(5), result.getString(6), result.getString(7));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		return emp;
	}

	public boolean regist(Emp emp) {

		ConnectionManager manager = new ConnectionManager();
		String sql = "insert into emp(empid, empname, mail, birthday, salary, password, deptid) values (?, ?, ?, ?, ?, ?, ?)";
		int result = 0;

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, emp.getEmpId());
			prepareStatement.setString(2, emp.getEmpName());
			prepareStatement.setString(3, emp.getMail());
			prepareStatement.setDate(4, emp.getBirthday());
			prepareStatement.setInt(5, emp.getSalary());
			prepareStatement.setString(6, emp.getPassword());
			prepareStatement.setString(7, emp.getDeptId());
			result = prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		if (result > 0)
			return true;
		else
			return false;
	}

	public boolean update(Emp emp) {

		ConnectionManager manager = new ConnectionManager();
		String sql = "update emp set empname = ?, mail = ?, birthday = ?, salary = ?, password = ?, deptid = ? where empid = ?";
		int result = 0;

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(7, emp.getEmpId());
			prepareStatement.setString(1, emp.getEmpName());
			prepareStatement.setString(2, emp.getMail());
			prepareStatement.setDate(3, emp.getBirthday());
			prepareStatement.setInt(4, emp.getSalary());
			prepareStatement.setString(5, emp.getPassword());
			prepareStatement.setString(6, emp.getDeptId());
			result = prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		if (result > 0)
			return true;
		else
			return false;

	}

	public boolean delete(String empId) {

		ConnectionManager manager = new ConnectionManager();
		String sql = "delete from emp where empid = ?";
		int result = 0;

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, empId);
			result = prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		if (result > 0)
			return true;
		else
			return false;

	}

}
