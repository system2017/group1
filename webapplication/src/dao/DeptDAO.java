package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import dto.Dept;
import util.ConnectionManager;

public class DeptDAO {

	public ArrayList<Dept> selectAll() {

		ConnectionManager manager = new ConnectionManager();
		ArrayList<Dept> list = new ArrayList<Dept>();
		String sql = "select * from dept order by deptid";

		try {
			ResultSet result = manager.getConnection().prepareStatement(sql).executeQuery();
			while (result.next())
				list.add(new Dept(result.getString(1), result.getString(2)));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		return list;
	}

	public Dept select(String deptId) {

		ConnectionManager manager = new ConnectionManager();
		Dept dept = null;
		String sql = "select * from dept where deptid = ? order by deptid";

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, deptId);
			ResultSet result = prepareStatement.executeQuery();
			if (result.next())
				dept = new Dept(result.getString(1), result.getString(2));
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		return dept;
	}

	public boolean regist(Dept dept) {

		ConnectionManager manager = new ConnectionManager();
		String sql = "insert into dept(deptid, deptname) values (?, ?)";
		int result = 0;

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, dept.getDeptId());
			prepareStatement.setString(2, dept.getDeptName());
			result = prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		if (result > 0)
			return true;
		else
			return false;
	}

	public boolean update(Dept dept) {

		ConnectionManager manager = new ConnectionManager();
		String sql = "update dept set deptname = ? where deptid = ?";
		int result = 0;

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(2, dept.getDeptId());
			prepareStatement.setString(1, dept.getDeptName());
			result = prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		if (result > 0)
			return true;
		else
			return false;

	}

	public boolean delete(String deptId) {

		ConnectionManager manager = new ConnectionManager();
		String sql = "delete from dept where deptid = ?";
		int result = 0;

		try {
			PreparedStatement prepareStatement = manager.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, deptId);
			result = prepareStatement.executeUpdate();
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			manager.closeConnection();
		}

		if (result > 0)
			return true;
		else
			return false;

	}

}
