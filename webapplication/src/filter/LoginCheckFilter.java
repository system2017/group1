package filter;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dto.Emp;

@WebFilter("/*")
public class LoginCheckFilter implements Filter {

	public LoginCheckFilter() {
	}

	public void destroy() {
	}

	@SuppressWarnings("unchecked")
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String path = ((HttpServletRequest) request).getServletPath();
		if (!path.equals("/login") && !path.equals("/logout") && !path.equals("/login_check")
				&& !path.equals("/css/style.css") && !path.equals("/css/topstyle.css")) {
			if (((HttpServletRequest) request).getSession(true).getAttribute("login_status") == null) {
				((HttpServletResponse) response).sendRedirect("/webapplication/login");
				return;
			} else {
				ArrayList<Emp> emplist = (ArrayList<Emp>) request.getServletContext().getAttribute("emp_list");
				boolean empcheck = false;
				String empid = ((Emp) ((HttpServletRequest) request).getSession(false).getAttribute("login_status"))
						.getEmpId();
				for (int i = 0; i < emplist.size(); i++) {
					if (emplist.get(i).getEmpId().equals(empid)) {
						empcheck = true;
						break;
					}
				}
				if (!empcheck) {
					((HttpServletRequest) request).getSession(false).invalidate();
					request.getRequestDispatcher("/jsp/Login.jsp").forward(request, response);
					return;
				}
			}
		}
		chain.doFilter(request, response);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
