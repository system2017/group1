package filter;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/*")
public class Filter implements javax.servlet.Filter {

	public Filter() {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html;charset=UTF-8");
		try {
			chain.doFilter(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getCause() instanceof SQLException) {
				((HttpServletResponse) response).sendRedirect("/webapplication/error");
				return;
			}
		}
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
