package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	private Connection connection;

	public Connection getConnection() {
		connection = null;
		try {
			connection = DriverManager.getConnection("jdbc:postgresql:webapplication", "webapplication",
					"webapplication");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return connection;
	}

	public void closeConnection() {
		if (connection != null)
			try {
				connection.close();
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
	}

}