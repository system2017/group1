<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>部署削除確認</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
<h1>部署削除</h1>
	<font color="red">${errm}</font>
	<c:if test="${dept_member == null }">
	<h2>以下の部署を削除しますか？</h2>
			<table class="tablestyle2">
				<tr>
					<th>部署ID</th>
					<td><c:out value="${detail_dept.deptId}"></c:out></td>
				</tr>
				<tr>
					<th>部署名</th>
					<td><c:out value="${detail_dept.deptName}"></c:out></td>
				</tr>
			</table>
			<table class="button">
				<tr>
					<td>
						<form action="/webapplication/dept_delete_complete" method="post">
								<input type="submit" value="削除する">
						</form>
					</td>
					</c:if>

					<td>
						<form action="/webapplication/dept_detail"method="post" >
							<input type="submit" value="キャンセル">
							<input type="hidden" name="dept_id" value="${detail_dept.deptId}">
						</form>
					</td>
				</tr>
			</table>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>