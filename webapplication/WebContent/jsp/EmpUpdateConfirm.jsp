<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員更新確認</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
	<h1>社員更新</h1>
	<h2>以下の内容で更新してよろしいでしょうか？</h2>
	<table class="tablestyle2">
		<tr>
			<th>社員ID</th>
			<td><c:out value="${detail_emp.empId}"></c:out></td>
		</tr>
		<tr>
			<th>社員名</th>
			<td><c:out value="${emp_name}"></c:out></td>
		</tr>
		<tr>
				<th>メールアドレス</th>
				<td><c:out value="${emp_mail}"></c:out></td>
			</tr>
			<tr>
				<th>生年月日</th>
				<td><c:out value="${emp_birthday}"></c:out></td>
			</tr>
			<tr>
				<th>給与</th>
				<td><c:out value="${emp_salary}"></c:out></td>
			</tr>
			<tr>
				<th>所属部署</th>
			<c:forEach var="v" items="${dept_list}" varStatus="i">
				<c:if test="${v.deptId.equals(emp_dept)}">
					<td>
					<c:out value="${v.deptName}"></c:out>
					</td>
				</c:if>
			</c:forEach>
			</tr>
			<tr>
				<th>パスワード変更</th>
				<td><c:out value="${passwd_change}"></c:out></td>
			</tr>
	</table>
	<table class="button"><tr>
	<td>
	<form action="/webapplication/emp_update_complete" method="post">
		<p>
			<input type="submit" value="確定">
			<input type="hidden" name="emp_name" value="<c:out value="${emp_name}"></c:out>">
			<input type="hidden" name="emp_mail" value="<c:out value="${emp_mail}"></c:out>">
			<input type="hidden" name="emp_birthday" value="<c:out value="${emp_birthday}"></c:out>">
			<input type="hidden" name="emp_salary" value="<c:out value="${emp_salary}"></c:out>">
			<input type="hidden" name="emp_dept" value="<c:out value="${emp_dept}"></c:out>">
			<input type="hidden" name="emp_password" value="<c:out value="${emp_password}"></c:out>">
			<input type="hidden" name="passwd_change" value="<c:out value="${passwd_change}"></c:out>">
		</p>
	</form></td>
	<td><form action="/webapplication/emp_update_cancel" method="post">
		<p>
			<input type="submit" value="キャンセル">
			<input type="hidden" name="emp_name" value="<c:out value="${emp_name}"></c:out>">
			<input type="hidden" name="emp_mail" value="<c:out value="${emp_mail}"></c:out>">
			<input type="hidden" name="emp_birthday" value="<c:out value="${emp_birthday}"></c:out>">
			<input type="hidden" name="emp_salary" value="<c:out value="${emp_salary}"></c:out>">
			<input type="hidden" name="emp_dept" value="<c:out value="${emp_dept}"></c:out>">
		</p>
	</form></td>
	</tr></table>
	</div>
	</div>
	 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>