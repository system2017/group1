<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員一覧</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
<h1>社員一覧</h1>
<font color="red"><c:out value="${errmsg}"></c:out></font>
<table class="tablestyle">
		<thead><tr>
			<th>社員ID</th>
			<th>社員名</th>
			<th></th>
		</tr></thead>
		<tbody>
		<c:forEach var="v" items="${emp_list}" varStatus="i">
		<tr>
			<td><c:out value="${v.empId}"></c:out></td>
			<td><c:out value="${v.empName}"></c:out></td>
			<td>
			<form action="/webapplication/emp_detail" method="post"><input type="submit" value="詳細" >
			<input type="hidden" name="emp_id" value="<c:out value="${v.empId}"></c:out>"></form>
			</td>
		</tr>
		</c:forEach>
	</table>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>