<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員更新</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
	<h1>社員更新</h1>
	<font color="red"><c:out value="${errmsg}"></c:out></font>
	<form action="/webapplication/emp_update_confirm" method="post">
		<table class="tablestyle2">
			<tr>
				<th>社員ID</th>
				<td><c:out value="${detail_emp.empId}"></c:out></td>
			</tr>
			<tr>
				<th>社員名<font size="1"  color="red">*</font></th>
				<td><input type="text" name="emp_name" pattern="([^\s].*[^\s])|([^\s]{1})" value="<c:out value="${emp_name}"></c:out>"></td>
			</tr>
			<tr>
				<th>メールアドレス<font size="1"  color="red">*</font></th>
				<td><input type="email" name="emp_mail" value="<c:out value="${emp_mail}"></c:out>"></td>
			</tr>
			<tr>
				<th>生年月日<font size="1"  color="red">*</font></th>
				<td><input type="date" name="emp_birthday" value="<c:out value="${emp_birthday}"></c:out>"></td>
			</tr>
			<tr>
				<th>給与<font size="1"  color="red">*</font></th>
				<td><input type="text" name="emp_salary" pattern="[1-9][0-9]*" value="<c:out value="${emp_salary}"></c:out>"></td>
			</tr>
			<tr>
				<th>所属部署</th>
				<td><select name="emp_dept">
					<c:forEach var="v" items="${dept_list}" varStatus="i">
						<option value="<c:out value="${v.deptId}"></c:out>"
						<c:if test="${emp_dept == v.deptId}">selected="selected"</c:if>>
						<c:out value="${v.deptName}"></c:out></option>
					</c:forEach>
				</select></td>
			</tr>
			<tr>
				<th>旧パスワード<font size="1"  color="red">*</font></th>
				<td><input type="password" name="emp_old_password" value=""></td>
			</tr>
			<tr>
				<th>新パスワード</th>
				<td><input type="password" name="emp_new_password" value="" pattern="[0-9A-Za-z]{8,}"></td>
			</tr>
			<tr>
				<th>新パスワード(確認)</th>
				<td><input type="password" name="emp_new_password_confirm" value=""></td>
			</tr>
		</table>
		<font size="2" ><font  color="red">*</font>がある項目は必須項目です。<br>
		パスワードは8文字以上英数字のみで入力してください
		</font>
			<table class="button">
		<tr>
			<td>
				<form action="/webapplication/emp_update_confirm" method="post">
					<input type="submit" value="更新する">
				</form>
			</td>
			<td><form action="/webapplication/emp_detail" method="post">
					<input type="submit" value="戻る">
					<input type="hidden" name="emp_id" value="<c:out value="${detail_emp.empId}"></c:out>">
				</form></td>
		</tr>
	</table>
	</form>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>