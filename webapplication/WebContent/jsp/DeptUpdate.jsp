<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>部署更新</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
	<h1>部署更新</h1>
	<font color="red"><c:out value="${errmsg}"></c:out></font>
	<form action="/webapplication/dept_update_confirm" method="post">
		<table class="tablestyle2">
			<tr>
				<th>部署ID</th>
				<td><c:out value="${detail_dept.deptId}"></c:out></td>
			</tr>
			<tr>
				<th>部署名<font size="1"  color="red">*</font></th>
				<td><input type="text" name="dept_name" pattern="([^\s].*[^\s])|([^\s]{1})" value="<c:out value="${dept_name}"></c:out>"></td>
			</tr>
		</table>
		<font size="2" ><font  color="red">*</font>がある項目は必須項目です。</font>
			<table class="button">
		<tr>
			<td>
				<form action="/webapplication/dept_update_complete" method="post">
					<input type="submit" value="更新する">
				</form>
			</td>
			<td><form action="/webapplication/dept_detail" method="post">
					<input type="submit" value="戻る">
					<input type="hidden" name="dept_id" value="<c:out value="${detail_dept.deptId}"></c:out>">
				</form></td>
		</tr>
	</table>
	</form>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>