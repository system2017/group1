<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員管理システム</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<div class="fadein">
<div class="login">
	<h1>社員管理システム</h1>
	<form action="/webapplication/login_check" method="post">
		<font color="red">${loginErr }</font>
		<p>
			ログインID<input type="text" name="name">
		</p>
		<p>
			パスワード<input type="password" name="password">
		</p>
		<p>
			<input type="submit" value="ログイン">
		</p>
	</form>
</div>
</div>
</body>
</html>