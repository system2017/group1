<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="menu">
	<h1>メニュー</h1>

<span class="marker">ようこそ<span class="buruburu-hover">卍</span><span class="buruburu-hover"><c:out value="${login_status.getEmpName()}"></c:out></span><span class="buruburu-hover">卍</span>さん</span>
	<ul>
		<li><a href="/webapplication/menu" class="mk">メインページ</a></li>
		<li><a href="/webapplication/emp_regist" class="mk">社員登録</a></li>
		<li><a href="/webapplication/emp_list" class="mk">社員一覧</a></li>
		<li><a href="/webapplication/dept_regist" class="mk">部署登録</a></li>
		<li><a href="/webapplication/dept_list" class="mk">部署一覧</a></li>
		<li><a href="/webapplication/logout" class="mk">ログアウト</a></li>
	</ul>
</div>