<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>部署詳細</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
	<h1>部署詳細</h1>
	<c:out value="${errmsg}"></c:out>
	<table class="tablestyle2">
		<tr>
			<th>部署ID</th>
			<td><c:out value="${detail_dept.deptId}"></c:out></td>
		</tr>
		<tr>
			<th>部署名</th>
			<td><c:out value="${detail_dept.deptName}"></c:out></td>
		</tr>
	</table>

	<table class="button">
		<tr>
			<td>
				<form action="/webapplication/dept_update" method="post">
					<input type="submit" value="更新"> <input type="hidden"
						name="dept_id"
						value="<c:out value="${detail_dept.deptId}"></c:out>">
				</form>
			</td>
			<td>
				<form action="/webapplication/dept_delete_confirm"
					method="post">
					<input type="submit" value="削除"> <input type="hidden"
						name="dept_id"
						value="<c:out value="${detail_dept.deptId}"></c:out>">
				</form></td>
			<td>
				<form action="/webapplication/dept_list"
					method="post">
					<input type="submit" value="一覧に戻る"> <input type="hidden"
						name="dept_id"
						value="<c:out value="${detail_dept.deptId}"></c:out>">
				</form></td>
		</tr>

	</table>

	<h1><c:out value="${detail_dept.deptName}"></c:out>社員</h1>
	<table class="tablestyle">
		<thead>
		<tr>
			<th>社員ID</th>
			<th>社員名</th>
			<th></th>
		</tr>
		</thead>
		<tbody>
		<c:forEach var="v" items="${emplist_bydept}" varStatus="i">
			<tr>
				<td><c:out value="${v.empId}"></c:out></td>
				<td><c:out value="${v.empName}"></c:out></td>
				<td>
					<form action="/webapplication/emp_detail" method="post">
						<input type="submit" value="詳細"> <input type="hidden"
							name="emp_id" value="<c:out value="${v.empId}"></c:out>">
					</form>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>