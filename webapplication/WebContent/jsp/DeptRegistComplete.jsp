<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>部署登録完了</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
	<h1>部署登録</h1>
	<h2>以下の内容で登録しました。</h2>
	<table class="tablestyle2">
		<tr>
			<th>部署ID</th>
			<td><c:out value="${dept.deptId}"></c:out></td>
		</tr>
		<tr>
			<th>部署名</th>
			<td><c:out value="${dept.deptName}"></c:out></td>
		</tr>
	</table>
<table class="button">
		<tr>
			<td>
			<form action="/webapplication/menu" >
				<p>
					<input type="submit" value="メインページへ">
				</p>
			</form>
			</td>

		</tr>
	</table>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>