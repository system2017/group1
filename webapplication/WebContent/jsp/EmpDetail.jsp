<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>社員詳細</title>
<link rel="stylesheet" href="css/topstyle.css">
</head>
<body>
<%@ include file="Menu.jsp"%>
<div class="fadein">
<div class="main">
	<h1>社員詳細</h1>
	<table class="tablestyle2">
		<tr>
			<th>社員ID</th>
			<td><c:out value="${detail_emp.empId}"></c:out></td>
		</tr>
		<tr>
			<th>社員名</th>
			<td><c:out value="${detail_emp.empName}"></c:out></td>
		</tr>
		<tr>
			<th>メールアドレス</th>
			<td><c:out value="${detail_emp.mail}"></c:out></td>
		</tr>
		<tr>
			<th>生年月日</th>
			<td><c:out value="${detail_emp.birthday}"></c:out></td>
		</tr>
		<tr>
			<th>給与</th>
			<td><c:out value="${detail_emp.salary}"></c:out></td>
		</tr>
		<tr>
			<th>部署</th>
			<c:forEach var="v" items="${dept_list}" varStatus="i">
				<c:if test="${v.deptId.equals(detail_emp.deptId)}">
					<td><c:out value="${v.deptName}"></c:out></td>
				</c:if>
			</c:forEach>
		</tr>
	</table>
<table class="button">
		<tr>
				<td>
				<form action="/webapplication/emp_update" method="post">
					<input type="submit" value="更新"> <input type="hidden"
						name="emp_id"
						value="<c:out value="${detail_emp.empId}"></c:out>">
				</form>
			</td>

				<td><form action="/webapplication/emp_delete"
					method="post">
					<input type="submit" value="削除"> <input type="hidden"
						name="emp_id"
						value="<c:out value="${detail_emp.empId}"></c:out>">
				</form></td>
			<td>
				<form action="/webapplication/emp_list"
					method="post">
					<input type="submit" value="一覧に戻る"> <input type="hidden"
						name="emp_id"
						value="<c:out value="${detail_emp.empId}"></c:out>">
				</form></td>

		</tr>
	</table>
</div>
</div>
 <div style="position: absolute; top: 320px; left: 0px; width: 37px;">
 <img src="pic/group_business.png" alt="サンプル" width="220" height="200" class="buruburu-hover" >
</div>
<div style="position: absolute; top: 300px; left: 0px; width: 37px;">
 <img src="pic/kyoutou.png" alt="サンプル" width="220" height="70" class="buruburu-hover">
</div>
</body>
</html>